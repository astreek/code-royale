source 'https://rubygems.org'

gem 'rails', '>= 5.0.0.beta3', '< 5.1'

# MySQL as database for Active Record
gem 'mysql2'

# Login with github
gem 'github_api'
gem 'octokit'

# Login with facebook
gem 'koala'

# Models validation
  # Email
  gem 'email_validator'
  # Date time
  gem 'validates_timeliness'

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Ace editor
gem 'ace-rails-ap'

# Friendly ID
gem 'friendly_id'

# Use Puma as the app server
gem 'puma'

# Use SCSS and bootstrap for stylesheets
gem 'bootstrap-sass'
gem 'sass-rails', '~> 5.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5.x'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'

# Use Capistrano for deployment
gem 'unicorn'
gem 'capistrano'
gem 'capistrano-rails', group: :development
gem 'rvm-capistrano'
gem 'capistrano3-unicorn'
gem 'dotenv-rails'
gem "capistrano-rvm"
gem 'capistrano-bundler'
gem "cap-ec2"
gem "capistrano-ext"


# Markdown text format to HTML
gem 'redcarpet'

# HTTP client
gem 'typhoeus'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 3.0'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
