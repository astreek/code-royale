# coderoyale
## A guide for new dev to get up and running

### 1. Prerequisites
- You have access to and can clone this repo
- You have the coderoyale Vagrant box file and config file (Vagrantfile)
- You have Vagrant and VirtualBox install on your machine

### 2. Setup steps
###### On Local machine
- Edit your hosts file to point `dev.coderoyale` to `localhost`
- Clone this repo to $coderoyale_folder
- Copy Vagrant box file and Vagrantfile to $coderoyale_folder
- `cd $coderoyale_folder`
- `vagrant up`

###### On guest machine
- Edit hosts file to point `compile.dev.coderoyale` to host machine

#### And... Voila !!

### 3. Usage
###### On guest machine you can
- `cd /vagrant` to access the repo source code folder (/vagrant is synced with $coderoyale_folder)
- `mysql -uroot` to access the database command line

###### On local machine you can
- Write code and do git things
- Access our app on http://dev.coderoyale:8081
