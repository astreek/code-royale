/***
 * Powering the ace editor throughout the app
 * Auto mode detection
 ***/

$(document).on('turbolinks:load', function() {
  $('div[data-ace-mode]').each(createAceEditor);
  function createAceEditor(idx) {
    var thisId = $(this).attr('id');
    var targetId = $(this).attr('data-ace-for');
    var targetMode = 'ace/mode/' + $(this).attr('data-ace-mode');
    var editor = ace.edit(thisId);
    var closestOuterForm = $(this).closest('form');

    editor.getSession().setMode(targetMode);
    editor.getSession().getDocument().setNewLineMode('unix');

    closestOuterForm.on("submit", changeHiddenValue);

    function changeHiddenValue() {
      $('#' + targetId).val(editor.getValue());
    }
  }
});
