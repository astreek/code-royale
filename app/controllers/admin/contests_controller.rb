class Admin::ContestsController < Admin::AdminController
  Contest = ::Contest

  def index
    @on_going = Contest.on_going
    @not_started = Contest.not_started
    @finished = Contest.finished
  end

  def new
    @contest = Contest.new
  end

  def create
    @contest = Contest.new(contest_params)
    @contest.creator = current_user
    if @contest.save
      redirect_to action: :index
    else
      render :new
    end
  end

  def edit
    @contest = this_contest
  end

  def update
    @contest = this_contest
    if @contest.update contest_params
      redirect_to action: :index
    else
      render :edit
    end
  end

  def destroy
    this_contest.problems.each do |problem|
      problem.available!
    end
    this_contest.problems.delete_all
    Contest.destroy params[:id]
    redirect_to action: :index
  end

  def problems
    @contest = this_contest
    @problems = this_contest.problems
    @available = Problem.available.belong_to_role Role.admin
  end

  def show_problem
    @contest = this_contest
    @problem = this_problem
    render :problem
  end

  def add_problem
    this_problem.published!
    this_contest.problems << this_problem
    redirect_to action: :problems
  end

  def delete_problem
    this_problem.available!
    this_contest.problems.delete this_problem
    redirect_to action: :problems
  end

  private
  def this_contest
    Contest.find(params[:id])
  end

  def this_problem
    Problem.find(params[:problem_id])
  end

  def contest_params
    params.require(:contest)
          .permit :title, :description, :start_time, :finish_time, :status
  end
end
