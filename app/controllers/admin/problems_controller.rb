class Admin::ProblemsController < Admin::AdminController
  Problem = ::Problem

  before_action :authority_check, only: [:show, :edit, :update, :destroy]

  def index
    admin = Role.admin
    @draft = Problem.draft.belong_to_role admin
    @available = Problem.available.belong_to_role admin
    @published = Problem.published.belong_to_role admin
  end

  def new
    @problem = Problem.new
  end

  def create
    @problem = Problem.new(problem_params)
    @problem.author = current_user
    if @problem.save
      redirect_to action: :show, id: @problem.id
    else
      render :new
    end
  end

  def show
    @problem = this_problem
  end

  def edit
    @problem = this_problem
  end

  def update
    @problem = this_problem
    if @problem.update problem_params
      redirect_to action: :show, id: @problem.id
    else
      render :edit
    end
  end

  def destroy
    Problem.destroy params[:id]
    redirect_to action: :index
  end

  private
  def this_problem
    Problem.find(params[:id])
  end

  def problem_params
    params.require(:problem).permit(:title, :max_points, :description, :status,
                                    :problem_type_id, :level_id, :sample_input,
                                    :sample_output, :explanation,
                                    domain_ids: [], language_ids: [])
  end

  def authority_check
    render file: 'public/404' unless this_problem.author.role == Role.admin
  end
end
