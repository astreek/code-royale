class Admin::TestCasesController < Admin::AdminController
  Problem = ::Problem
  TestCase = ::TestCase

  before_action :authority_check, only: [:edit, :update, :destroy]

  def index
    @problem = this_problem
    @test_cases = this_problem.test_cases
  end

  def new
    @problem = this_problem
    @test_case = TestCase.new
  end

  def create
    @problem = this_problem
    @test_case = TestCase.new(test_case_params)
    @test_case.problem = @problem
    if @test_case.save
      redirect_to action: :index, problem_id: @problem.id
    else
      render :new
    end
  end

  def edit
    @problem = this_problem
    @test_case = this_test_case
    redirect_to action: :index unless this_test_case.of? this_problem
  end

  def update
    @problem = this_problem
    @test_case = this_test_case
    redirect_to action: :index unless this_test_case.of? this_problem
    if @test_case.update test_case_params
      redirect_to action: :index, problem_id: @problem.id
    else
      render :edit
    end
  end

  def destroy
    TestCase.destroy this_test_case
    redirect_to action: :index, problem_id: params[:problem_id]
  end

  private
  def this_problem
    Problem.find(params[:problem_id])
  end

  def this_test_case
    TestCase.find(params[:id])
  end

  def test_case_params
    params.require(:test_case)
      .permit :problem_id, :input, :output, :max_time, :secret
  end

  def authority_check
    render file: 'public/404' unless this_test_case.author_role == Role.admin
  end
end
