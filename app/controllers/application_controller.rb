class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :logged_in, :current_user, :custom_config

  def login(user)
    session[:user_id] = user.id
  end

  def logout
    session.delete :user_id
    @current_user = nil
    redirect_to root_path
  end

  private
  def custom_config
    Rails.configuration.x
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if logged_in
  end

  def admin_only
    redirect_to main_path unless (current_user && current_user.admin?)
  end

  def org_only
    redirect_to main_path unless (current_user && current_user.org?)
  end

  def user_only
    redirect_to root_path unless logged_in
  end

  def guest_only
    redirect_to main_path if logged_in
  end

  def logged_in
    session[:user_id] && User.find_by(id: session[:user_id])
  end
end
