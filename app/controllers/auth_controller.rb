class AuthController < ApplicationController
  before_action :guest_only

  def landing
    @user = User.new
  end

  def new
    @user = User.new
    render :register
  end

  def register
    user = User.new(regist_params)
    if user.save
      login(user)
      redirect_to main_path
    else
      @user = user
      render :register
    end
  end

  def authenticate
    user = User.find_by(email: login_email)
    if user && user.authenticate(login_password)
      login(user)
      redirect_to main_path
    else
      @user = user || User.new(email: login_email)
      render :landing
    end
  end

  private
  def regist_params
    params[:user]
      .permit :email, :name, :password, :password_confirmation
  end

  def login_email
    params[:user][:email]
  end

  def login_password
    params[:user][:password]
  end
end
