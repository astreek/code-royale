class ContestsController < ApplicationController
  before_action :user_only

  def index
    @on_going = Contest.published.on_going
    @not_started = Contest.published.not_started
    @finished = Contest.published.finished
  end

  def show
    @contest = this_contest
    redirect_to contests_path unless @contest.published?
  end

  def join
    @contest = this_contest
    if current_user.can_join?(@contest)
      @contest.joined_users << current_user
    end
    redirect_to contest_problems_path(@contest)
  end

  def problems
    @contest = this_contest
    redirect_to contest_path(@contest) unless current_user.can_view_details?(@contest)
  end

  private
  def this_contest
    Contest.find(params[:id])
  end

end
