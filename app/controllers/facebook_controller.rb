class FacebookController < Oauth2Controller

  private
  def client
    @client ||= Koala::Facebook::OAuth.new(
      custom_config.facebook.app_id,
      custom_config.facebook.app_secret,
      "#{custom_config.domain}#{facebook_callback_path}"
    )
  end

  def get_auth_url
    client.url_for_oauth_code(permissions: "public_profile email")
  end

  def facebook_user
    @facebook_user ||= @graph.get_object('me', fields: "id,email,name,link,picture{url}")
  end

  def get_info
    if params[:code]
      @graph ||= Koala::Facebook::API.new(client.get_access_token(params[:code]))
      facebook_user
    end
  end

  def get_registered_profile
    @register_profile ||= FacebookProfile.find_by(id: facebook_user['id'])
  end

  def create_profile
    profile_info = facebook_user.select do |key, value|
      ['id', 'email', 'name', 'link'].include? key
    end
    profile_info['avatar_url'] = facebook_user['picture']['data']['url']
    FacebookProfile.new(profile_info)
  end

end
