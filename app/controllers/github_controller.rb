class GithubController < Oauth2Controller

  private
  def gitapp
    @gitapp ||= Github.new(
      client_id: custom_config.github.app_id,
      client_secret: custom_config.github.app_secret
    )
  end

  def get_auth_url
    gitapp.authorize_url(
      redirect_uri: "#{custom_config.domain}#{github_callback_path}",
      scope: 'user:email'
    )
  end

  def code
    params[:code]
  end

  def github_client
    token = gitapp.get_token(code).token
    @client ||= Octokit::Client.new(access_token: token)
  end

  def github_user
    @github_user ||= github_client.user
  end

  def get_info
    github_user if code
  end

  def get_registered_profile
    @registered_profile ||= GithubProfile.find_by(id: github_user.id)
  end

  def get_github_info(attr_array)
    github_user.to_hash.select do |key, value|
      attr_array.include?(key)
    end
  end

  def create_profile
    g_info = get_github_info [:id, :email, :html_url, :name, :avatar_url]
    GithubProfile.new(g_info)
  end

end
