class HomeController < ApplicationController
  before_action :guest_only, only: [:landing]
  before_action :user_only, only: [:main]

  def main
    @current_user = current_user
  end
end
