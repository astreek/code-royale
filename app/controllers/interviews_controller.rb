class InterviewsController < ApplicationController
  before_action :published_interview_only
  before_action :can_auth, only: [:auth, :start]
  before_action :can_do_problems, except: [:show, :auth, :start, :result, :logout]
  before_action :can_finish, only: [:result, :logout]
  before_action :valid_problem, only: [:show_problem, :try_problem, :solve_problem, :submission_result]

  def show
    @interview = this_interview
  end

  def auth
    @interview = this_interview
    @interviewee = Interviewee.new
  end

  def start
    @interviewee = Interviewee.find_by interviewee_params.merge({interview_id: this_interview.id})
    if @interviewee
      login @interviewee
      @interviewee.interview_result.start
      redirect_to action: :problems
    else
      @interviewee = Interviewee.new email: interviewee_params[:email]
      render :auth
    end
  end

  def problems
    @interview = this_interview
    @problems = @interview.problems
    @submissions = current_interviewee.submissions.index_by &:problem_id
  end

  def show_problem
    @interview = this_interview
    @problem = this_problem
    @submission = last_submission || InterviewSubmission.new(problem: @problem)
  end

  def try_problem
    @submission = InterviewSubmission.new(submission_params)
    @submission.test(secret: false)
    @test_results = @submission.test_results
  end

  def solve_problem
    last = last_submission
    @submission = InterviewSubmission.new(submission_params)
    @submission.author = current_interviewee
    if @submission.process
      last.destroy if last
      redirect_to action: :submission_result
    else
      @interview = this_interview
      @problem = this_problem
      render :show_problem, status: :internal_server_error
    end
  end

  def submission_result
    @interview = this_interview
    @problem = this_problem
    @submission = last_submission
  end

  def finish
    current_interviewee.interview_result.finish
    redirect_to action: :result
  end

  def result
    @interviewee = current_interviewee
  end

  def logout
    clear
    redirect_to action: :show
  end

  protected
  def published_interview_only
    render file: 'public/404' unless this_interview.published?
  end

  def can_auth
    if (interviewee? && of_this_interview?)
      redirect_to action: :problems if !current_interviewee.interview_result.finished?
      redirect_to action: :result if current_interviewee.interview_result.finished?
    end
  end

  def can_do_problems
    if (interviewee? && of_this_interview?)
      redirect_to action: :result if current_interviewee.interview_result.finished?
    else
      redirect_to action: :auth
    end
  end

  def can_finish
    if (interviewee? && of_this_interview?)
      redirect_to action: :problems unless current_interviewee.interview_result.finished?
    else
      redirect_to action: :auth
    end
  end

  def valid_problem
    raise ActiveRecord::RecordNotFound.new unless this_problem.of? this_interview    
  end

  private
  def this_interview
    Interview.friendly.find(params[:interview_id])
  end

  def this_problem
    Problem.find(params[:problem_id])
  end

  def last_submission
    InterviewSubmission.find_by(problem: this_problem, author: current_interviewee)
  end

  def submission_params
    params.require(:submission).permit(:problem_id, :language_id, :content)
  end

  def interviewee?
    session[:interviewee_id] && Interviewee.find_by(id: session[:interviewee_id])
  end

  def of_this_interview?
    current_interviewee.interview == this_interview
  end

  def interviewee_params
    params.require(:interviewee)
      .permit :email, :password
  end

  def current_interviewee
    Interviewee.find_by(id: session[:interviewee_id])
  end

  def login interviewee
    session[:interviewee_id] = interviewee.id
  end

  def clear
    session.delete :interviewee_id
    @current_interviewee = nil
  end

end
