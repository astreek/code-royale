class Oauth2Controller < ApplicationController
  before_action :guest_only

  def authorize
    redirect_to get_auth_url
  end

  def callback
    if get_info
      user = registered_user || register
      if user
        login(user)
        redirect_to main_path and return
      end
    end
    redirect_to root_path
  end

  protected
  def registered_user
    registered_profile = get_registered_profile
    registered_profile.user if registered_profile
  end

  def register
    @new_profile = create_profile
    user = get_linkable_user || create_user
    @new_profile.user = user
    User.transaction do
      if user.save && @new_profile.save
        user
      else
        raise ActiveRecord::Rollback
      end
    end
  end

  def get_linkable_user(profile = @new_profile)
    User.find_by(email: profile.email) if profile.email
  end

  def create_user
    user_info = {
      email: @new_profile.email,
      name: @new_profile.name,
      avatar_url: @new_profile.avatar_url,
    }
    User.new(user_info)
  end

  def get_auth_url
  end

  def get_info
  end

  def get_registered_profile
  end

  def create_profile
  end

end
