class Org::CompanyProfilesController < Org::OrganizationController
  CompanyProfile = ::CompanyProfile

  before_action :authority_check

  def edit
    @company = this_company
  end

  def update
    company = this_company
    if company.update company_params
      redirect_to org_path
    else
      @company = company
      render :edit
    end
  end

  private
  def this_company
    CompanyProfile.find(params[:id])
  end

  def company_params
    params[:company_profile]
      .permit :name
  end

  def authority_check
    render file: 'public/404' unless this_company.owner == current_user
  end
end
