class Org::HomeController < Org::OrganizationController
  def main
    @company = current_company
  end

  private
  def current_company
    if company = current_user.company_profile
      company
    else
      company = CompanyProfile.new(owner_id: current_user.id)
      company.save
      company
    end
  end
end
