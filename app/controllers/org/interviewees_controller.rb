class Org::IntervieweesController < Org::OrganizationController
  before_action :authority_check
  before_action :valid_container_check, only: [:show, :problem, :problem_result]
  before_action :valid_container_for_problem_check, only: [:problem, :problem_result]

  def index
    @interviewees = Interviewee.belongs_to_interview this_interview
    @interview = this_interview
  end

  def new
    @interviewee = Interviewee.new
    @interview = this_interview
  end

  def create
    @interviewee = Interviewee.create interviewee_params
    @interview = this_interview
    @interviewee.interview = this_interview
    if @interviewee.save
      redirect_to action: :index
    else
      render :new
    end
  end

  def show
    @interview = this_interview
    @interviewee = this_interviewee
    @interview_result = @interviewee.interview_result
    @problems = @interview.problems
    @submissions = @interviewee.submissions.index_by &:problem_id
  end

  def problem
    @interview = this_interview
    @problem = this_problem
    @interviewee = this_interviewee
  end

  def problem_result
    @interview = this_interview
    @problem = this_problem
    @interviewee = this_interviewee
    @submission = @interviewee.submissions.find_by({problem_id: @problem.id})
  end

  def destroy
    Interviewee.destroy params[:id]
    redirect_to action: :index
  end

  private
  def this_interview
    Interview.friendly.find(params[:interview_id])
  end

  def this_interviewee
    Interviewee.find(params[:id])
  end

  def interviewee_params
    params.require(:interviewee)
      .permit :email
  end

  def this_problem
    Problem.find(params[:p_id])
  end

  def authority_check
    render file: 'public/404' unless this_interview.owner == current_user
  end

  def valid_container_check
    render file: 'public/404' unless this_interviewee.of? this_interview
  end

  def valid_container_for_problem_check
    render file: 'public/404' unless this_problem.of? this_interview
  end
end
