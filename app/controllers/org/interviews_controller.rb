class Org::InterviewsController < Org::OrganizationController
  Interview = ::Interview
  before_action :authority_check, except: [:index, :new, :create]
  before_action :valid_container_check, only: [:delete_problem]

  def index
    @interviews = Interview.of current_user
  end

  def new
    @interview = Interview.new
  end

  def create
    @interview = Interview.new(interview_params)
    @interview.owner = current_user
    if @interview.save
      redirect_to action: :show, id: @interview.friendly_id
    else
      render :new
    end
  end

  def show
    @interview = this_interview
  end

  def edit
    @interview = this_interview
  end

  def update
    @interview = this_interview
    if @interview.update interview_params
      redirect_to action: :show, id: @interview.friendly_id
    else
      render :edit
    end
  end

  def problems
    @interview = this_interview
    @problems = this_interview.problems
    @available = Problem.available.belong_to_user current_user
  end

  def show_problem
    @interview = this_interview
    @problem = this_problem
    render :problem
  end

  def add_problem
    if this_problem.ready? && this_interview.draft?
      this_interview.problems << this_problem
    end
    redirect_to action: :problems
  end

  def delete_problem
    this_interview.problems.delete this_problem unless this_interview.published?
    redirect_to action: :problems
  end

  def destroy
    this_interview.problems.delete_all
    Interview.friendly.destroy params[:id]
    redirect_to action: :index
  end

  private
  def this_interview
    Interview.friendly.find(params[:id])
  end

  def this_problem
    Problem.find(params[:problem_id])
  end

  def interview_params
    params[:interview]
      .permit :title, :description, :salary_min, :salary_max, :duration, :status
  end

  def authority_check
    render file: 'public/404' unless this_interview.owner == current_user
  end

  def valid_container_check
    render file: 'public/404' unless this_problem.of? this_interview
  end
end
