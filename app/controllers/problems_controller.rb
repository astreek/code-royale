class ProblemsController < ApplicationController

  def show
    @problem = this_problem
    @submission = UserSubmission.new
  end

  private
  def this_problem
    Problem.find(params[:id])
  end

end
