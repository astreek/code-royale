class SubmissionsController < ApplicationController
  before_action :user_only

  def create
    @submission = UserSubmission.new(submission_params)
    @submission.author = current_user
    if @submission.process
      redirect_to action: :show, id: @submission.id
    else
      render :show
    end
  end

  def show
    @submission = this_submission
  end

  private
  def this_submission
    UserSubmission.find(params[:id])
  end

  def submission_params
    params.require(:user_submission).permit(:problem_id, :language_id, :content)
  end

  def this_problem
    Problem.find(params[:problem_id])
  end

end
