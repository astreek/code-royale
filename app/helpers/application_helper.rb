module ApplicationHelper
  def can_change_status? thing
    !thing.published?
  end
end
