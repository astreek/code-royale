module ContestHelper
  def contest_allowed_statuses
    Contest.statuses.keys.to_a
  end
end
