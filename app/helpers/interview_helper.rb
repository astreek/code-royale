module InterviewHelper
  def interview_allowed_statuses
    Interview.statuses.keys.to_a
  end

  def interview_url interview
    "#{custom_config.domain}/interviews/#{interview.friendly_id}"
  end

  def company_name_of interview
    interview.owner.company_profile.name if interview.owner.company_profile
  end

  def interview_submission_status submission
    if submission && submission.score == submission.problem.max_points
      "DONE"
    else
      "NOT DONE"
    end
  end

  def interview_submission_status_class submission
    if submission
      if submission.score == submission.problem.max_points
        "label-info"
      else
        "label-default"
      end
    else
      "label-danger"
    end
  end

  def interview_submission_score submission, problem
    score_text = sprintf('%d / %d', submission ? submission.score : 0, problem.max_points)
    link_to_if(submission, score_text, interview_problem_result_path(@interview, problem))
  end

  def org_interview_submission_score submission, problem
    score_text = sprintf('%d / %d', submission ? submission.score : 0, problem.max_points)
    link_to_if(submission, score_text, org_interview_interviewee_problem_result_path(@interview, @interviewee, problem))
  end
end
