module InterviewResultHelper
  def used_time_of interview
    if interview.used_time
      "#{interview.used_time.to_i / 60} Minutes #{interview.used_time.to_i % 60} Seconds."
    else
      "Not Done"
    end
  end
end
