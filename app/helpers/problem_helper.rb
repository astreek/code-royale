module ProblemHelper
  def problem_allowed_statuses
    Problem.statuses.keys.to_a[0..1]
  end
end
