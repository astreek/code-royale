module SubmissionHelper

	def get_ace_modes(langs = nil)
    langs = Language.all unless langs
    modes = {
      "c" => "c_cpp",
      "java" => "java",
      "php" => "php",
      "js" => "javascript",
      "cpp" => "c_cpp",
      "python2" => "python",
      "python3" => "python",
      "ruby" => "ruby",
      "scala" => "scala",
      "swift" => "swift",
      "objc" => "objectivec",
    }
    Hash[ langs.map { |l| [l.id, (modes.include?(l.codename) ? "ace/mode/" + modes[l.codename] : "")] } ].to_json
  end

end
