module TestResultHelper

  def format_test_result(test_result)
    case
    when test_result.passed?
      'Passed'
    when test_result.failed?
      'Failed'
    when test_result.timed_out?
      'Execution time limit exceeded'
    when test_result.error_occurred?
      'Error occurred'
    when test_result.unavailable?
      'Unavailable'
    else
      'Unknown'
    end
  end

end
