class CodeExecutor

  def initialize(language, code)
    @language = language
    @source_code = code
  end

  def exec(input)
    body = { language: @language.codename, code: @source_code, stdin: input }
    response = Typhoeus.post(Rails.configuration.x.compilebox.url,
                             headers: {'Content-Type' => 'application/json'}, 
                             body: body.to_json)
    if !response.success?
      nil   # fail to get any result
            # should retry here
    else
      result = JSON.parse(response.body)
      if result['time'] == ""
        ExecutionResult.new(result['output'], result['errors'])
      elsif result['errors'] == ""
        ExecutionResult.new(result['output'], nil, result['time'].to_f)
      elsif result['output'] == "Compilation Failed"
        ExecutionResult.new("", result['errors'], 0)
      else
        ExecutionResult.new(result['output'], result['errors'], result['time'].to_f)
      end
    end
  end

end
