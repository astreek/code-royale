class ExecutionResult
  attr_reader :output
  attr_reader :error
  attr_reader :time

  def initialize(output, error = nil, time = nil)
    @output = output
    @error = error
    @time = time
  end

  def finished?
    !timed_out? && (!@error || @error == "")
  end

  def timed_out?
    @time == nil
  end

end
