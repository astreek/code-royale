class TestRunner

  def self.run(executor, test_cases, with_output: false)
    test_cases.map do |test|
      exec_result = executor.exec(test.input)
      test_result = create_test_result(exec_result, test)
      test_result.test_case = test
      if with_output
        test_result.output = exec_result.output if exec_result
      end
      test_result
    end
  end

  private
  def self.create_test_result(exec_result, test_case)
    if !exec_result
      TestResult.new(result: :unavailable)
    elsif exec_result.finished?
      if test_case.correct_output?(exec_result.output)
        TestResult.new(result: :passed, time: exec_result.time)
      else
        TestResult.new(result: :failed, time: exec_result.time)
      end
    elsif exec_result.timed_out?
      TestResult.new(result: :timed_out)
    else 
      TestResult.new(result: :error_occurred)
    end
  end

end
