module TextParser
  @@markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML.new(escape_html: true),
                         no_intra_emphasis: true, superscript: true, highlight: true)

  private
  def parse_md(md_text)
    @@markdown.render(md_text)
  end
end
