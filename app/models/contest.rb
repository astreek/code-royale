class Contest < ApplicationRecord

  include TextParser

  belongs_to :creator, class_name: 'User', foreign_key: 'creator_id'
  has_many :contest_joins
  has_many :joined_users, through: :contest_joins, source: :user
  has_many :problems

  validates :title, :description, :start_time, :finish_time, presence: true
  validates_datetime :start_time, on: :create, on_or_after: :now, type: :date
  validates_datetime :finish_time, after: :start_time, type: :date

  enum status: [:draft, :published]

  scope :not_started, -> { where("start_time > ?", Time.current) }

  scope :on_going, -> { where("start_time <= ? AND finish_time > ?", Time.current, Time.current) }

  scope :finished, -> { where("finish_time <= ?", Time.current) }


  def not_started?
    start_time > Time.current
  end

  def on_going?
    !(not_started? || finished?)
  end

  def finished?
    finish_time <= Time.current
  end

  def description=(value)
    super
    self.description_html = parse_md(value)
    value
  end

  def explanation=(value)
    super
    self.explanation_html = parse_md(value)
    value
  end

end
