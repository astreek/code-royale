class FacebookProfile < ApplicationRecord
  belongs_to :user

  validates :id, :email, :name, :link, presence: true
  validates :email, email: true

end
