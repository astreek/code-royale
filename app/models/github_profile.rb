class GithubProfile < ApplicationRecord
  belongs_to :user

  validates :id, :email, :html_url, :avatar_url, presence: true
  validates :email, email: true

end
