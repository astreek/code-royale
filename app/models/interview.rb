class Interview < ApplicationRecord
  include TextParser
  include FriendlyId

  belongs_to :owner, class_name: 'User', foreign_key: :owner_id

  has_many :interview_joins
  has_many :joined_users, through: :interview_joins, source: :user

  has_many :interviewees, dependent: :destroy, before_add: :check_duplicate_email

  has_many :problems, before_add: :publish, before_remove: :unpublish

  enum status: [:draft, :published]

  friendly_id :friendly_name, use: [:slugged]

  validates :title, :description, :salary_min, :salary_max, :duration, presence: true

  scope :of, ->(owner) { where(owner_id: owner.id) }

  def should_generate_new_friendly_id?
    new_record?
  end

  def description=(value)
    super
    self.description_html = parse_md(value)
    value
  end

  private
  def friendly_name
    owner_name = self.owner.company_profile.name ? self.owner.company_profile.name : "coderoyale"

    pool = [('a'..'z'), ('A'.. 'Z'), (0..9)]
            .map { |a_range| a_range.to_a }
            .flatten
    random_text = pool.sample(24).join('')

    "#{owner_name}-#{random_text}"
  end

  def publish problem
    problem.published!
  end

  def unpublish problem
    problem.available!
  end
  def check_duplicate_email interviewee

  end
end
