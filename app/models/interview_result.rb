class InterviewResult < ApplicationRecord
  belongs_to :interviewee, optional: false

  def start
    self.start_time = DateTime.current unless started?
    save
  end

  def finish
    calculate_score
    self.finish_time = DateTime.current unless finished?
    save
  end

  def finished?
    !!finish_time
  end

  def started?
    !!start_time
  end

  def status
    finished? ? 'Done' : 'Not Done'
  end

  def used_time
    if finished?
      finish_time - start_time
    else
      nil
    end
  end

  private
  def calculate_score
    self.score = interviewee.submissions.sum(&:score) unless score
  end
end
