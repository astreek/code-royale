class InterviewSubmission < Submission
  belongs_to :author, class_name: 'Interviewee', foreign_key: 'interviewee_id'
end
