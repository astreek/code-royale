class Interviewee < ApplicationRecord

  belongs_to :interview
  has_one :interview_result, dependent: :destroy
  has_many :submissions, class_name: 'InterviewSubmission', dependent: :destroy

  validates :email, presence: true

  before_create :add_random_password, :add_interview_result

  scope :belongs_to_interview, -> (interview) { where(interview_id: interview.id) }

  def of? interview
    self.interview == interview
  end

  private
  DEFAULT_PASSWORD_LENGTH = 6
  def add_random_password
    pool = [('a'..'z'), ('A'.. 'Z'), (0..9)]
            .map { |a_range| a_range.to_a }
            .flatten
    self.password = pool.sample(DEFAULT_PASSWORD_LENGTH).join('')
  end

  def add_interview_result
    self.interview_result = InterviewResult.new
  end
end
