class Level < ApplicationRecord
  has_many :problems, dependent: :destroy
end
