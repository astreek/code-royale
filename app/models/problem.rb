class Problem < ApplicationRecord

  include TextParser

  after_initialize :enable_all_languages

  has_many :test_cases, dependent: :destroy

  belongs_to :type, class_name: 'ProblemType', foreign_key: 'problem_type_id'
  belongs_to :level
  belongs_to :contest, optional: true
  belongs_to :interview, optional: true
  has_and_belongs_to_many :languages
  has_and_belongs_to_many :domains

  belongs_to :author, class_name: 'User', foreign_key: 'author_id'
  has_many :submissions, dependent: :destroy

  validates :title, :description, :type, presence: true

  enum status: [:draft, :available, :published]

  scope :belong_to_role, -> (role) { joins(:author).where("users.role_id = ?", role.id) }
  scope :belong_to_user, -> (user) { where("author_id = ?", user.id) }

  def description=(value)
    super
    self.description_html = parse_md(value)
    value
  end

  def explanation=(value)
    super
    self.explanation_html = parse_md(value)
    value
  end

  def of? interview
    self.interview == interview
  end

  def ready?
    !test_cases.count.zero? && available?
  end

  private
  def enable_all_languages
    if self.new_record? && languages.empty?
      self.languages = Language.all
    end
  end

end
