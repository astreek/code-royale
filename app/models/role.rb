class Role < ApplicationRecord
  has_many :users

  GENERAL_ID = 1
  ADMIN_ID = 2
  ORG_ID = 3

  def self.general
    find(GENERAL_ID)
  end

  def self.admin
    find(ADMIN_ID)
  end

  def self.org
    find(ORG_ID)
  end

  def general?
    id == GENERAL_ID
  end

  def admin?
    id == ADMIN_ID
  end

  def org?
    id == ORG_ID
  end
end
