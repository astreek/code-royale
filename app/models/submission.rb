class Submission < ApplicationRecord

  self.inheritance_column = 'class_type'

  belongs_to :problem
  belongs_to :language

  has_many :test_results, dependent: :destroy

  validate :all_test_cases_run
  validate :no_bad_test_result

  def new_executor
    CodeExecutor.new language, content
  end

  def test(custom_test_cases = nil, secret: true)
    executor = new_executor
    results = TestRunner.run(executor, secret ? problem.test_cases : problem.test_cases.visible, with_output: !secret)
    results += TestRunner.run(executor, custom_test_cases, with_output: true) if custom_test_cases
    self.test_results = results
    results.none? &:bad?
  end

  def calc_score
    passed_num = test_results.to_a.count { |r| r.passed? }
    last_score = passed_num.fdiv(problem.test_cases.size) * problem.max_points
    self.score = last_score.to_i
  end

  def process
    if test
      calc_score
      save
    else
      false
    end
  end

  def accepted
    all_test_cases_run && test_results.all?(&:passed)
  end

  private
  def no_bad_test_result
    errors.add(:test_results, 'are unavailable') if test_results.any?(&:bad?)
  end

  def all_test_cases_run
    if problem && !(problem.test_cases - test_results.map(&:test_case)).empty?
      errors.add(:test_results, 'are missing')
    end
  end

end
