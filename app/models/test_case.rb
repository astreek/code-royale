class TestCase < ApplicationRecord
  belongs_to :problem

  has_many :test_results, dependent: :destroy

  scope :secret,  -> { where(secret: true) }
  scope :visible, -> { where(secret: false) }

  def correct_output?(out)
    output.chomp == out.chomp
  end

  def author
    problem.author
  end

  def author_role
    problem.author.role
  end

  def of?(problem)
    self.problem == problem
  end

end
