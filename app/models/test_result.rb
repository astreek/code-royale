class TestResult < ApplicationRecord
  belongs_to :submission
  belongs_to :test_case

  attr_accessor :output

  validates :result, presence: true

  validate do |this|
    errors.add(:base) if bad?
  end

  enum result: { passed: 1, failed: 0, timed_out: -1, error_occurred: -2, unavailable: -99 }

  def bad?
    # unavailable?
    false
  end

end
