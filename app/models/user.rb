class User < ApplicationRecord
  DEFAULT_PASSWORD_LENGTH = 6

  after_initialize :add_default_role, :add_random_password

  belongs_to :role

  has_one :github_profile, dependent: :destroy
  has_one :facebook_profile, dependent: :destroy
  has_one :company_profile, required: false, dependent: :destroy, foreign_key: :owner_id

  has_many :contributed_problems , class_name: 'Problem', foreign_key: :author_id

  has_many :created_contests, class_name: 'Contest', foreign_key: :creator_id
  has_many :contest_joins
  has_many :joined_contests, through: :contest_joins, source: :contest
  has_many :submissions, class_name: 'UserSubmission', foreign_key: :author_id

  has_many :own_interviews, class_name: 'Interview', foreign_key: :owner_id
  has_many :interview_joins
  has_many :joined_interviews, through: :interview_joins, source: :interview

  has_secure_password
  validates :password, length: { minimum: 6 }, on: :create
  validates :email, presence: true
  validates :email, uniqueness: true
  validates :email, email: true

  def general_user?
    role.general?
  end

  def admin?
    role.admin?
  end

  def org?
    role.org?
  end

  def in_contest?(contest)
    joined_contests.include? contest
  end

  def can_join?(contest)
    contest.published? && !contest.finished? && !in_contest?(contest)
  end

  def can_view_details?(contest)
    contest.published? && (contest.finished? || in_contest?(contest))
  end

  private
  def self.random_password(len=DEFAULT_PASSWORD_LENGTH)
    pool = [('a'..'z'), ('A'.. 'Z'), (0..9)]
            .map { |a_range| a_range.to_a }
            .flatten
    pool.sample(len).join('')
  end

  def add_default_role
    self.role = Role.general if (self.new_record? && self.role == nil)
  end

  def add_random_password
    self.password = User.random_password if (self.new_record? && self.password == nil)
  end
end
