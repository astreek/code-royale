json.try_problem true
json.passed_num @test_results.to_a.count { |r| r.passed? }
json.test_results @test_results do |test_result|
  json.extract! test_result, :result, :output
end
