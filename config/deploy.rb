set :user, 'coderoyale'
set :application, 'coderoyale'
set :deploy_to, '~/app'

set :repo_url, 'git@bitbucket.org:nalvn/coderoyale.git'
set :scm, :git
set :deploy_via, :remote_cache
set :keep_releases, 5

set :rvm_ruby_version, '2.3.1@coderoyale'

set :linked_dirs, %w(bin log tmp/pids
                     tmp/cache tmp/sockets
                     vendor/bundle public/system public/assets)

set :linked_files, %w(.env)

set :ec2_region, %w(ap-northeast-1)

ssh_options = {
  user: 'coderoyale',
  auth_methods: %w(publickey)
}

set :sidekiq_role, :app

#ec2_role :app, ssh_options: ssh_options
#ec2_role :db, ssh_options: ssh_options, primary: true
#ec2_role :deploy, ssh_options: ssh_options

namespace :deploy do

  desc 'Run the precompile assets locally and rsync them to servers'
  task :precompile do
    %x(RAILS_ENV=#{fetch(:stage)} bundle exec rake assets:precompile)
    on roles :app, :deploy do
      rsync_host = host.to_s
      run_locally do
        execute "rsync -av --delete ./public/assets/ #{fetch(:user)}@#{rsync_host}:#{shared_path}/public/assets/"
      end
    end
    %x(rm -rf public/assets)
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :groups, limit: 5, wait: 5 do
      invoke 'unicorn:legacy_restart'
    end
  end

  before :updated, :precompile
  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end

