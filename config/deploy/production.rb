set :rails_env, 'production'
set :unicorn_rack_env, 'production'
set :branch, 'master'
server '153.122.50.13', roles: %w{web app db batch}
