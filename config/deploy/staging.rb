set :rails_env, 'staging'
set :unicorn_rack_env, 'staging'
set :branch, 'master'
server '153.122.54.41', user: 'coderoyale', roles: %w{web app db batch}
