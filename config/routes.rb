Rails.application.routes.draw do
  # Landing page
  root to: 'auth#landing'

  # Register and login related
    # Github
  get 'github-auth', to: 'github#authorize'
  get 'github-callback', to: 'github#callback'

    # Facebook
  get 'facebook-auth', to: 'facebook#authorize'
  get 'facebook-callback', to: 'facebook#callback'

    # Email
  get 'register', to: 'auth#new'
  post 'register', to: 'auth#register'
  post 'email-auth', to: 'auth#authenticate'
    # Logout
  get 'logout', to: 'application#logout'

  # General user section
  get 'main', to: 'home#main'

    # Problems
  resources :problems, only: [:index, :show]

    # Submission
  resources :submissions, only: [:show, :create]
  post 'problems/:problem_id/submit', to: 'submissions#create', as: 'solve_problem'

    # Contest
  resources :contests, only: [:index, :show]
  get 'contests/join/:id', to: 'contests#join', as: 'join_contest'
  get 'contests/:id/problems', to: 'contests#problems', as: 'contest_problems'

  resources :interviews, only: [] do
    nested do
      get '', action: 'show', as: ''
      get 'auth'
      post 'start'
      post 'finish'
      get 'result'
      post 'logout'
      get 'problems'
      scope 'problems/:problem_id', as: 'problem' do
        get '', action: 'show_problem', as: 'show'
        post 'try', action: 'try_problem', defaults: { format: 'json' }
        post 'solve', action: 'solve_problem'
        get 'result', action: 'submission_result'
      end
    end
  end

  # Admin page
  namespace :admin do
    root 'home#main', as: ''
    resources :problems do
      resources :test_cases, except: [:show]
    end
    resources :contests
    get 'contests/:id/problems', to: 'contests#problems', as: 'contest_problems'
    get 'contests/:id/problems/:problem_id', to: 'contests#show_problem', as: 'contest_show_problem'
    delete 'contests/:id/problems/:problem_id', to: 'contests#delete_problem', as: 'contest_delete_problem'
    post 'contests/:id/problems/:problem_id', to: 'contests#add_problem', as: 'contest_add_problem'
  end

  # Organization page
  namespace :org do
    root 'home#main', as: ''
    resources :company_profiles, only: [:edit, :update]
    resources :interviews do
      resources :interviewees, only: [:index, :new, :create, :show, :destroy]
    end
    get 'interviews/:interview_id/interviewee/:id/problems/:p_id', to: 'interviewees#problem', as: 'interview_interviewee_problem'
    get 'interviews/:interview_id/interviewee/:id/problems/:p_id/result', to: 'interviewees#problem_result', as: 'interview_interviewee_problem_result'
    get 'interviews/:id/problems', to: 'interviews#problems', as: 'interview_problems'
    get 'interviews/:id/problems/:problem_id', to: 'interviews#show_problem', as: 'interview_show_problem'
    delete 'interviews/:id/problems/:problem_id', to: 'interviews#delete_problem', as: 'interview_delete_problem'
    post 'interviews/:id/problems/:problem_id', to: 'interviews#add_problem', as: 'interview_add_problem'
    resources :problems do
      resources :test_cases
    end
  end

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
