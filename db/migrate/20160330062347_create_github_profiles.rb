class CreateGithubProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :github_profiles do |t|
      t.string :uid
      t.string :email
      t.string :html_url
      t.string :avatar_url
      t.string :name

      t.timestamps
    end
  end
end
