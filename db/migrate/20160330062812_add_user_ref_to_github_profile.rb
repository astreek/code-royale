class AddUserRefToGithubProfile < ActiveRecord::Migration[5.0]
  def change
    add_reference :github_profiles, :user, foreign_key: true
  end
end
