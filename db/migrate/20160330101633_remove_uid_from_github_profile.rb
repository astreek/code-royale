class RemoveUidFromGithubProfile < ActiveRecord::Migration[5.0]
  def change
    remove_column :github_profiles, :uid
  end
end
