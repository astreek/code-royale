class AddEmailToFacebookProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :facebook_profiles, :email, :string
    add_column :facebook_profiles, :name, :string
    add_column :facebook_profiles, :link, :string
  end
end
