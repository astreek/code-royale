class FixIdInFacebookProfile < ActiveRecord::Migration[5.0]
  def change
    change_column :facebook_profiles, :id, :string
  end
end
