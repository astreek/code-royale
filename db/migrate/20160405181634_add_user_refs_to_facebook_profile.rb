class AddUserRefsToFacebookProfile < ActiveRecord::Migration[5.0]
  def change
    add_reference :facebook_profiles, :user, foreign_key: true
  end
end
