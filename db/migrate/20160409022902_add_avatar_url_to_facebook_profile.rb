class AddAvatarUrlToFacebookProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :facebook_profiles, :avatar_url, :string
  end
end
