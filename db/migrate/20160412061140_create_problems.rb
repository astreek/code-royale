class CreateProblems < ActiveRecord::Migration[5.0]
  def change
    create_table :problems do |t|
      t.string :title
      t.text :contents, limit: 4294967295
      t.integer :max_points
      t.integer :author_id

      t.belongs_to :problem_type, index: true
      t.belongs_to :level, index: true

      t.timestamps
    end
    add_foreign_key :problems, :users, column: :author_id
  end
end
