class CreateTestCases < ActiveRecord::Migration[5.0]
  def change
    create_table :test_cases do |t|
      t.text :input, limit: 16777215
      t.text :output, limit: 16777215

      t.belongs_to :problem, index: true

      t.timestamps
    end
  end
end
