class CreateDomainsProblems < ActiveRecord::Migration[5.0]
  def change
    create_table :domains_problems, id: false do |t|
      t.belongs_to :problem, index: true
      t.belongs_to :domain, index: true
    end
  end
end
