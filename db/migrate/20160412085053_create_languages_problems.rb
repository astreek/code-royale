class CreateLanguagesProblems < ActiveRecord::Migration[5.0]
  def change
    create_table :languages_problems, id: false do |t|
      t.belongs_to :problem, index: true
      t.belongs_to :language, index: true
    end
  end
end
