class CreateContests < ActiveRecord::Migration[5.0]
  def change
    create_table :contests do |t|
      t.string :title
      t.string :description
      t.string :text
      t.datetime :start_time
      t.datetime :finish_time
      t.integer :creator_id

      t.timestamps
    end
    add_foreign_key :contests, :users, column: :creator_id
  end
end
