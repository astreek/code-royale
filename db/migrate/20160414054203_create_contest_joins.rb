class CreateContestJoins < ActiveRecord::Migration[5.0]
  def change
    create_table :contest_joins do |t|
      t.datetime :joined_time
      t.belongs_to :user, index: true
      t.belongs_to :contest, index: true

      t.timestamps
    end
  end
end
