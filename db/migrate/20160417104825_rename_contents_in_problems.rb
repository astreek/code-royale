class RenameContentsInProblems < ActiveRecord::Migration[5.0]
  def change
    rename_column :problems, :contents, :description
  end
end
