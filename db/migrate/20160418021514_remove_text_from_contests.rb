class RemoveTextFromContests < ActiveRecord::Migration[5.0]
  def change
    change_table :contests do |t|
      t.remove :text, :description
      t.text :description
    end
  end
end
