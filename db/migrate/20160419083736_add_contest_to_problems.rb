class AddContestToProblems < ActiveRecord::Migration[5.0]
  def change
    add_reference :problems, :contest, foreign_key: true, index: true
  end
end
