class AddFieldsToProblems < ActiveRecord::Migration[5.0]
  def change
    add_column :problems, :sample_input, :string
    add_column :problems, :sample_output, :string
    add_column :problems, :explanation, :text, limit: 4294967295
    add_column :problems, :explanation_html, :text, limit: 4294967295
  end
end
