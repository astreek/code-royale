class AddPublishedToProblems < ActiveRecord::Migration[5.0]
  def change
    add_column :problems, :published, :boolean, default: false
  end
end
