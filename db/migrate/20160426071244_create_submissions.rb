class CreateSubmissions < ActiveRecord::Migration[5.0]
  def change
    create_table :submissions do |t|
      t.text :content, limit: 16777215
      t.integer :score

      t.belongs_to :problem
      t.belongs_to :language
      t.integer :author_id
      t.belongs_to :contest

      t.timestamps
    end
    add_foreign_key :submissions, :users, column: :author_id
  end
end
