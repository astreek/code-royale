class CreateTestResults < ActiveRecord::Migration[5.0]
  def change
    create_table :test_results do |t|
      t.belongs_to :submission
      t.belongs_to :test_case
      t.integer :result

      t.timestamps
    end
  end
end
