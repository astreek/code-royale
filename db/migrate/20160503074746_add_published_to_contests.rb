class AddPublishedToContests < ActiveRecord::Migration[5.0]
  def change
    add_column :contests, :published, :boolean, default: false
  end
end
