class AddTimeToTestResult < ActiveRecord::Migration[5.0]
  def change
    add_column :test_results, :time, :float
  end
end
