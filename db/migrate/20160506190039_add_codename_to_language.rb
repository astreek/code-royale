class AddCodenameToLanguage < ActiveRecord::Migration[5.0]
  def change
    add_column :languages, :codename, :string
  end
end
