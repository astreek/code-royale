class AddMaxTimeToTestCases < ActiveRecord::Migration[5.0]
  def change
    add_column :test_cases, :max_time, :float
    add_column :test_cases, :sample, :boolean, default: false, null: false
  end
end
