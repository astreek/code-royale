class AddDescriptionHtmlToContest < ActiveRecord::Migration[5.0]
  def change
    add_column :contests, :description_html, :text, limit: 4294967295
  end
end
