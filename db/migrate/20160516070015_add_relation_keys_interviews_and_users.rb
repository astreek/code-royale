class AddRelationKeysInterviewsAndUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :interviews, :owner_id, :integer
    add_foreign_key :interviews, :users, column: :owner_id

    add_reference :interview_joins, :user, foreign_key: true, index: true
    add_reference :interview_joins, :interview, foreign_key: true, index: true
  end
end
