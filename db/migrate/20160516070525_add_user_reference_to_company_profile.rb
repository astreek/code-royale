class AddUserReferenceToCompanyProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :company_profiles, :owner_id, :integer
    add_foreign_key :company_profiles, :users, column: :owner_id
  end
end
