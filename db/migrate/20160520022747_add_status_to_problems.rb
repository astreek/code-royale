class AddStatusToProblems < ActiveRecord::Migration[5.0]
  def change
    add_column :problems, :status, :integer, default: 0
    remove_column :problems, :published
  end
end
