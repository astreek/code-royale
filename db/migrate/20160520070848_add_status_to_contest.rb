class AddStatusToContest < ActiveRecord::Migration[5.0]
  def change
    add_column :contests, :status, :integer, default: 0
    remove_column :contests, :published
  end
end
