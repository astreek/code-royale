class AddInfoToInterviews < ActiveRecord::Migration[5.0]
  def change
    add_column :interviews, :description, :text
    add_column :interviews, :description_html, :text
    add_column :interviews, :salary_min, :integer
    add_column :interviews, :salary_max, :integer
    add_column :interviews, :duration, :integer
  end
end
