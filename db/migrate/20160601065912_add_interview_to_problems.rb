class AddInterviewToProblems < ActiveRecord::Migration[5.0]
  def change
    add_reference :problems, :interview, foreign_key: true, index: true
  end
end
