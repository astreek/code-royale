class AddStatusToInterview < ActiveRecord::Migration[5.0]
  def change
    add_column :interviews, :status, :integer, default: 0
  end
end
