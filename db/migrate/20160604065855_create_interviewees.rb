class CreateInterviewees < ActiveRecord::Migration[5.0]
  def change
    create_table :interviewees do |t|
      t.string :email
      t.string :password

      t.timestamps
    end
    add_reference :interviewees, :interview, foreign_key: true, index: true
  end
end
