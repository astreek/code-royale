class AddClassTypeToSubmission < ActiveRecord::Migration[5.0]
  def change
    add_column :submissions, :class_type, :string
  end
end
