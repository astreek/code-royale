class AddIntervieweeToSubmission < ActiveRecord::Migration[5.0]
  def change
    add_reference :submissions, :interviewee, foreign_key: true, index: true
  end
end
