class CreateInterviewResults < ActiveRecord::Migration[5.0]
  def change
    create_table :interview_results do |t|
      t.datetime :start_time
      t.datetime :finish_time
      t.integer :score

      t.timestamps
    end

    add_reference :interview_results, :interviewee, foreign_key: true, index: true
  end
end
