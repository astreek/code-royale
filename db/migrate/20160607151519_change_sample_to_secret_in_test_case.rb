class ChangeSampleToSecretInTestCase < ActiveRecord::Migration[5.0]
  def change
    rename_column :test_cases, :sample, :secret
    change_column_default :test_cases, :secret, from: false, to: true

    # correcting data as per new semantic
    TestCase.update_all('secret = not secret')
  end
end
