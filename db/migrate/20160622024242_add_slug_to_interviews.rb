class AddSlugToInterviews < ActiveRecord::Migration[5.0]
  def change
    add_column :interviews, :slug, :string
    add_index :interviews, :slug
  end
end
