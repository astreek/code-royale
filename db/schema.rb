# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160622024242) do

  create_table "company_profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "owner_id"
  end

  add_index "company_profiles", ["owner_id"], name: "fk_rails_01c34a743c", using: :btree

  create_table "contest_joins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "joined_time"
    t.integer  "user_id"
    t.integer  "contest_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "contest_joins", ["contest_id"], name: "index_contest_joins_on_contest_id", using: :btree
  add_index "contest_joins", ["user_id"], name: "index_contest_joins_on_user_id", using: :btree

  create_table "contests", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.datetime "start_time"
    t.datetime "finish_time"
    t.integer  "creator_id"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.text     "description",      limit: 65535
    t.text     "description_html", limit: 4294967295
    t.integer  "status",                              default: 0
  end

  add_index "contests", ["creator_id"], name: "fk_rails_98043f51ed", using: :btree

  create_table "domains", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "domains_problems", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "problem_id"
    t.integer "domain_id"
  end

  add_index "domains_problems", ["domain_id"], name: "index_domains_problems_on_domain_id", using: :btree
  add_index "domains_problems", ["problem_id"], name: "index_domains_problems_on_problem_id", using: :btree

  create_table "facebook_profiles", id: :string, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "email"
    t.string   "name"
    t.string   "link"
    t.integer  "user_id"
    t.string   "avatar_url"
  end

  add_index "facebook_profiles", ["user_id"], name: "index_facebook_profiles_on_user_id", using: :btree

  create_table "github_profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email"
    t.string   "html_url"
    t.string   "avatar_url"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  add_index "github_profiles", ["user_id"], name: "index_github_profiles_on_user_id", using: :btree

  create_table "interview_joins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "user_id"
    t.integer  "interview_id"
  end

  add_index "interview_joins", ["interview_id"], name: "index_interview_joins_on_interview_id", using: :btree
  add_index "interview_joins", ["user_id"], name: "index_interview_joins_on_user_id", using: :btree

  create_table "interview_results", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "start_time"
    t.datetime "finish_time"
    t.integer  "score"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "interviewee_id"
  end

  add_index "interview_results", ["interviewee_id"], name: "index_interview_results_on_interviewee_id", using: :btree

  create_table "interviewees", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email"
    t.string   "password"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "interview_id"
  end

  add_index "interviewees", ["interview_id"], name: "index_interviewees_on_interview_id", using: :btree

  create_table "interviews", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "owner_id"
    t.text     "description",      limit: 65535
    t.text     "description_html", limit: 65535
    t.integer  "salary_min"
    t.integer  "salary_max"
    t.integer  "duration"
    t.integer  "status",                         default: 0
    t.string   "slug"
  end

  add_index "interviews", ["owner_id"], name: "fk_rails_de89440ea2", using: :btree
  add_index "interviews", ["slug"], name: "index_interviews_on_slug", using: :btree

  create_table "languages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "codename"
  end

  create_table "languages_problems", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "problem_id"
    t.integer "language_id"
  end

  add_index "languages_problems", ["language_id"], name: "index_languages_problems_on_language_id", using: :btree
  add_index "languages_problems", ["problem_id"], name: "index_languages_problems_on_problem_id", using: :btree

  create_table "levels", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "problem_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "problems", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.text     "description",      limit: 4294967295
    t.integer  "max_points"
    t.integer  "author_id"
    t.integer  "problem_type_id"
    t.integer  "level_id"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.text     "description_html", limit: 4294967295
    t.integer  "contest_id"
    t.string   "sample_input"
    t.string   "sample_output"
    t.text     "explanation",      limit: 4294967295
    t.text     "explanation_html", limit: 4294967295
    t.integer  "status",                              default: 0
    t.integer  "interview_id"
  end

  add_index "problems", ["author_id"], name: "fk_rails_e8a9a7d3d1", using: :btree
  add_index "problems", ["contest_id"], name: "index_problems_on_contest_id", using: :btree
  add_index "problems", ["interview_id"], name: "index_problems_on_interview_id", using: :btree
  add_index "problems", ["level_id"], name: "index_problems_on_level_id", using: :btree
  add_index "problems", ["problem_type_id"], name: "index_problems_on_problem_type_id", using: :btree

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "submissions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text     "content",        limit: 16777215
    t.integer  "score"
    t.integer  "problem_id"
    t.integer  "language_id"
    t.integer  "author_id"
    t.integer  "contest_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "class_type"
    t.integer  "interviewee_id"
  end

  add_index "submissions", ["author_id"], name: "fk_rails_be7142e265", using: :btree
  add_index "submissions", ["contest_id"], name: "index_submissions_on_contest_id", using: :btree
  add_index "submissions", ["interviewee_id"], name: "index_submissions_on_interviewee_id", using: :btree
  add_index "submissions", ["language_id"], name: "index_submissions_on_language_id", using: :btree
  add_index "submissions", ["problem_id"], name: "index_submissions_on_problem_id", using: :btree

  create_table "test_cases", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text     "input",      limit: 16777215
    t.text     "output",     limit: 16777215
    t.integer  "problem_id"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.float    "max_time",   limit: 24
    t.boolean  "secret",                      default: true, null: false
  end

  add_index "test_cases", ["problem_id"], name: "index_test_cases_on_problem_id", using: :btree

  create_table "test_results", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "submission_id"
    t.integer  "test_case_id"
    t.integer  "result"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.float    "time",          limit: 24
  end

  add_index "test_results", ["submission_id"], name: "index_test_results_on_submission_id", using: :btree
  add_index "test_results", ["test_case_id"], name: "index_test_results_on_test_case_id", using: :btree

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email"
    t.string   "password_digest"
    t.string   "name"
    t.string   "avatar_url"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "role_id"
  end

  add_index "users", ["role_id"], name: "index_users_on_role_id", using: :btree

  add_foreign_key "company_profiles", "users", column: "owner_id"
  add_foreign_key "contests", "users", column: "creator_id"
  add_foreign_key "facebook_profiles", "users"
  add_foreign_key "github_profiles", "users"
  add_foreign_key "interview_joins", "interviews"
  add_foreign_key "interview_joins", "users"
  add_foreign_key "interview_results", "interviewees"
  add_foreign_key "interviewees", "interviews"
  add_foreign_key "interviews", "users", column: "owner_id"
  add_foreign_key "problems", "contests"
  add_foreign_key "problems", "interviews"
  add_foreign_key "problems", "users", column: "author_id"
  add_foreign_key "submissions", "interviewees"
  add_foreign_key "submissions", "users", column: "author_id"
  add_foreign_key "users", "roles"
end
