# Bring DB back to stone age
ActiveRecord::Base.establish_connection
ActiveRecord::Base.connection.tables.each do |table|
  ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
  ActiveRecord::Base.connection.execute("TRUNCATE #{table}") unless table == "schema_migrations"
  ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
end


Role.create([
    {name: 'General', description: 'Normal users who come to code.'},
    {name: 'Admin', description: 'Administrator. Can manage general users, problems and contest.'},
    {name: 'Organization', description: 'Organization. Can manage interviews and problems for those interviews.'}
])

Level.create([
    {name: 'Easy'},
    {name: 'Medium'},
    {name: 'Hard'}
])

ProblemType.create([
    {name: 'Coding'},
    {name: 'Multichoice'}
])

Domain.create([
  {name: 'Algorithm'},
  {name: 'Data Structure'}
])

Language.create([
    {name: 'C', codename: 'c'},
    {name: 'Java', codename: 'java'},
    {name: 'PHP', codename: 'php'},
    {name: 'Javascript', codename: 'js'},
    {name: 'C++', codename: 'cpp'},
    {name: 'Python2', codename: 'python2'},
    {name: 'Python3', codename: 'python3'},
    {name: 'Ruby', codename: 'ruby'},
    {name: 'Scala', codename: 'scala'},
    {name: 'Swift', codename: 'swift'},
    {name: 'ObjectiveC', codename: 'objc'},
])

User.create([
    {name: 'Admin', email: 'admin@nal.vn', password: '12345sau', role_id: 2},
    {name: 'Nal', email: 'nal@nal.vn', password: '12345sau', role_id: 3},
])
