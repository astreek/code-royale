require 'test_helper'

class AuthControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get auth_new_url
    assert_response :success
  end

  test "should get register" do
    get auth_register_url
    assert_response :success
  end

  test "should get authenticate" do
    get auth_authenticate_url
    assert_response :success
  end

end
